package com.example.firebase;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class InsertNoteActivity extends AppCompatActivity implements NoteAdapter.NoteClickListener, View.OnClickListener {

    private LinearLayout resultLayout;
    private NoteAdapter noteAdapter;
    private List<Note> noteList = new ArrayList<>();
    private DatabaseReference databaseNotes;
    private String selectedNoteId;
    private FirebaseAuth mAuth;
    private RelativeLayout recyclerLayout;
    private LinearLayout detailNotesLayout;
    private RecyclerView rvNotes;
    private EditText etTitle, etDescription;
    private TextView tvEmail, tvUid;
    private Button btnTambah, btnSubmit, btnDelete, btnKeluar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_note);

        mAuth = FirebaseAuth.getInstance();
        databaseNotes = FirebaseDatabase.getInstance().getReference("notes");

        btnKeluar.setOnClickListener(this);
        btnKeluar = findViewById(R.id.btn_keluar);
        tvEmail = findViewById(R.id.tv_email);
        tvUid = findViewById(R.id.tv_uid);

        resultLayout = findViewById(R.id.result_layout);
        recyclerLayout = findViewById(R.id.recycler_layout);
        detailNotesLayout = findViewById(R.id.detail_notes);
        rvNotes = findViewById(R.id.rv_notes);
        etTitle = findViewById(R.id.et_title);
        etDescription = findViewById(R.id.et_description);
        btnTambah = findViewById(R.id.btn_tambah);
        btnSubmit = findViewById(R.id.btn_submit);
        btnDelete = findViewById(R.id.btn_delete);

        btnTambah.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        btnDelete.setOnClickListener(this);

        noteAdapter = new NoteAdapter(noteList, this);
        rvNotes.setLayoutManager(new LinearLayoutManager(this));
        rvNotes.setAdapter(noteAdapter);

        // Atur tampilan default
        detailNotesLayout.setVisibility(View.GONE);
        recyclerLayout.setVisibility(View.VISIBLE);

        loadNotes();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            tvEmail.setText(currentUser.getEmail());
            tvUid.setText(currentUser.getUid());
        }
    }

    private void saveOrUpdateNote() {
        String title = etTitle.getText().toString().trim();
        String description = etDescription.getText().toString().trim();

        if (TextUtils.isEmpty(title)) {
            etTitle.setError("Title is required");
            return;
        }

        if (TextUtils.isEmpty(description)) {
            etDescription.setError("Description is required");
            return;
        }

        if (selectedNoteId == null) {
            String userId = mAuth.getCurrentUser().getUid();
            String noteId = databaseNotes.child(userId).push().getKey();
            Note note = new Note(noteId, title, description);
            if (noteId != null) {
                databaseNotes.child(userId).child(noteId).setValue(note);
            }
        } else {
            String userId = mAuth.getCurrentUser().getUid();
            Note note = new Note(selectedNoteId, title, description);
            databaseNotes.child(userId).child(selectedNoteId).setValue(note);
            selectedNoteId = null;
        }

        detailNotesLayout.setVisibility(View.GONE);
        recyclerLayout.setVisibility(View.VISIBLE);
    }

    private void deleteNote() {
        if (selectedNoteId != null) {
            String userId = mAuth.getCurrentUser().getUid();
            databaseNotes.child(userId).child(selectedNoteId).removeValue();
            selectedNoteId = null;
        }

        detailNotesLayout.setVisibility(View.GONE);
        recyclerLayout.setVisibility(View.VISIBLE);
    }

    private void loadNotes() {
        databaseNotes.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                noteList.clear();
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    for (DataSnapshot noteSnapshot : userSnapshot.getChildren()) {
                        Note note = noteSnapshot.getValue(Note.class);
                        noteList.add(note);
                    }
                }
                noteAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Handle possible errors.
            }
        });
    }

    public void logout() {
        mAuth.signOut();
        Intent intent = new Intent(InsertNoteActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); // make sure user can't go back
        startActivity(intent);
    }

    @Override
    public void onNoteClick(int position) {
        Note note = noteList.get(position);
        selectedNoteId = note.getId();
        etTitle.setText(note.getTitle());
        etDescription.setText(note.getDescription());
        recyclerLayout.setVisibility(View.GONE);
        detailNotesLayout.setVisibility(View.VISIBLE);
        btnDelete.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.btn_keluar) {
            logout();
        } else if (id == R.id.btn_tambah) {
            recyclerLayout.setVisibility(View.GONE);
            detailNotesLayout.setVisibility(View.VISIBLE);
            btnDelete.setVisibility(View.GONE);
            etTitle.setText("");
            etDescription.setText("");
        } else if (id == R.id.btn_submit) {
            saveOrUpdateNote();
        } else if (id == R.id.btn_delete) {
            deleteNote();
        }
    }
}
